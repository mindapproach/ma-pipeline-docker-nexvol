FROM       busybox
MAINTAINER Bernd Fischer <bfischer@mindapproach.de>

RUN mkdir /sonatype-work
RUN chown default /sonatype-work
RUN chmod 750 /sonatype-work
